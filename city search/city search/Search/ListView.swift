//
//  SwiftUIView.swift
//  city search
//
//  Created by Bence Fulop on 21/10/21.
//

import SwiftUI

struct ListView: View {
    @ObservedObject var fetch = DataManager()
    @State var searchBarText = String()
    
    var searchResults: [Country] {
        if searchBarText.count < 3 {
            return fetch.countries
        } else {
            return fetch.countries.filter { $0.name.contains(searchBarText) || $0.native.contains(searchBarText)  }
        }
    }
    
    var body: some View {
        List(searchResults) { country in
            NavigationLink(destination: DetailView(country: country)) {
                CountryCard(country: country)
            }
        }
        .searchable(text: $searchBarText,
                    placement: .navigationBarDrawer(displayMode: .always),
                    prompt: "France, Barbados... ") // only available in iOS 15
        .navigationBarTitle("Country Search")
        .navigationBarTitleDisplayMode(.large)
    }
}


struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
