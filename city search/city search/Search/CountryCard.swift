//
//  CountryCard.swift
//  city search
//
//  Created by Bence Fulop on 28/10/21.
//

import SwiftUI

struct CountryCard: View {
    
    var country: Country
    
    var body: some View {
        HStack {
            Text(country.emoji)
                .font(.system(size: 30))
            VStack(alignment: .leading, spacing: 3) {
                Text(country.name)
                    .fontWeight(.semibold)
                    .minimumScaleFactor(0.5) // Automatically shrink text on smaller displays
                    .font(.title3)
                Text(country.native)
                    .font(.subheadline)
                    .foregroundColor(Color.gray)
            }
        }
        .padding()
        .frame(height: 80)
    }
}


struct CountryCard_Previews: PreviewProvider {
    static var previews: some View {
        let country = DataManager().countries.first!
        CountryCard(country: country)
    }
}
