//
//  city_searchApp.swift
//  city search
//
//  Created by Bence Fulop on 17/10/21.
//

import SwiftUI

@main
struct city_searchApp: App {
    
    var body: some Scene {
        WindowGroup {
            LandingView()
        }
    }
}
