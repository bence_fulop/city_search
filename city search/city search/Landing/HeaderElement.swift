//
//  HeaderElement.swift
//  city search
//
//  Created by Bence Fulop on 28/10/21.
//

import SwiftUI

struct HeaderElement: View {
    var body: some View {
        VStack {
            Text("Country\nSearch")
                .font(.system(size: 80, weight: .heavy, design: .default))
                .foregroundColor(Color.white)
        }
        .frame(width: 370, alignment: .leading)
    }
}
