//
//  ContentView.swift
//  city search
//
//  Created by Bence Fulop on 17/10/21.
//

import SwiftUI

struct LandingView: View {
    
    var body: some View {
        NavigationView {
            ZStack {
                PlayerView()
                    .ignoresSafeArea()
                    .overlay(Color.red.opacity(0.1))
                VStack {
                    HeaderElement()
                    Spacer()
                    NavigationLink(destination: ListView()) {
                        StartButton()
                    }
                    Spacer()
                    FooterElement()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        LandingView()
    }
}
