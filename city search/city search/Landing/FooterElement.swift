//
//  FooterElement.swift
//  city search
//
//  Created by Bence Fulop on 28/10/21.
//

import SwiftUI

struct FooterElement: View {
    var body: some View {
        Text("Built with 😘 by Bence Fulop")
            .font(.footnote)
            .fontWeight(.light)
            .padding(.vertical)
            .foregroundColor(.white)
    }
}
