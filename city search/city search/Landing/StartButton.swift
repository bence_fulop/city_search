//
//  StartButton.swift
//  city search
//
//  Created by Bence Fulop on 28/10/21.
//

import SwiftUI

struct StartButton: View {
    var body: some View {
        Text("Start")
            .font(.system(size: 50))
            .fontWeight(.heavy)
            .foregroundColor(.white)
            .padding(.horizontal)
            .background(Color.red)
            .cornerRadius(10)
    }
}
