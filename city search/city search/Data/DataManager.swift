//
//  DataManager.swift
//  city search
//
//  Created by Bence Fulop on 24/10/21.
//

import Foundation

class DataManager: ObservableObject {
    @Published var countries = [Country]()
    
    init() {
        
        if let url = Bundle.main.url(forResource: "countries.json", withExtension: nil){
            if let data = try? Data(contentsOf: url) {
                let jsondecoder = JSONDecoder()
                do {
                    let countriesOfTheWorld = try jsondecoder.decode(CountryCode.self, from: data)
                    for country in countriesOfTheWorld.countries.values {
                        let newCountry = Country(
                            id: UUID(),
                            name: country.name,
                            native: country.native,
                            phoneExtension: country.phoneExtension,
                            continent: country.continent,
                            capital: country.capital,
                            currency: country.currency,
                            languages: country.languages,
                            emoji: country.emoji,
                            emojiUTF: country.emojiUTF
                        )
                        self.countries.append(newCountry)
                    }
                    self.countries = sortedCountries()
                }
                catch {
                    print("error trying parse json: \(error)")
                }
            }
        }
    }
    
    func sortedCountries() -> [Country] {
        return self.countries.sorted { (first: Country, second: Country) -> Bool in
            return first.name.caseInsensitiveCompare(second.name) == .orderedAscending
        }
    }
}


