//
//  CountryModel.swift
//  city search
//
//  Created by Bence Fulop on 23/10/21.
//

import Foundation

struct CountryCode: Codable {
    let countries: [String: Country]
    
    enum CodingKeys: String, CodingKey {
        case countries = "country"
    }
}

struct Country: Codable, Identifiable {
    public var id = UUID()
    public let name: String
    public let native: String
    public let phoneExtension: String
    public let continent: String
    public let capital: String
    public let currency: String
    public let languages: [String]
    public let emoji: String
    public let emojiUTF: String
    
    
    enum CodingKeys: String, CodingKey {
        case name, native
        case phoneExtension =  "phone"
        case continent, capital, currency, languages, emoji
        case emojiUTF = "emojiU"
    }
}
