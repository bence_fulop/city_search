//
//  OutlineButton.swift
//  city search
//
//  Created by Bence Fulop on 28/10/21.
//

import SwiftUI

struct OutlineButton: View {
    let countryName: String
    
    var countryNameNoSpaces: String {
        countryName.replacingOccurrences(of: " ", with: "_")
    }
    
    let buttonText: String
    
    var body: some View {
        Link(destination: (URL(string: "maps://?q=\(countryNameNoSpaces)"))! , label: {
            Text(buttonText)
                .bold()
                .font(.title2)
                .frame(width: 180, height: 50, alignment: .center)
                .foregroundColor(Color.red)
                .border(.red, width: 5)
                .cornerRadius(5)
        })
    }
}


struct OutlineButton_Previews: PreviewProvider {
    static var previews: some View {
        OutlineButton(countryName: "Germany", buttonText: "See on map")
    }
}
