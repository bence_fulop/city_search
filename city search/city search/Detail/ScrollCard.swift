//
//  ScrollCard.swift
//  city search
//
//  Created by Bence Fulop on 28/10/21.
//

import SwiftUI

struct ScrollCard: View {
    let cardText: String
    let iconImage: String
    let cardColour: Color
    let textColor: Color

    var body: some View {
        Label(cardText, systemImage: iconImage)
            .labelStyle(VerticalLabelStyle())
            .scaledToFit()
            .frame(minWidth: 150, minHeight: 150)
            .background(cardColour)
            .foregroundColor(textColor)
            .cornerRadius(10)
            .font(.system(size: 25))
            .lineLimit(2)
    }
}


struct ScrollCard_Previews: PreviewProvider {
    static var previews: some View {
        let country = DataManager().countries.first!
        ScrollCard(cardText: country.capital, iconImage: "globe.europe.africa.fill", cardColour: .red, textColor: .white)
    }
}
