//
//  HorizontalScrollView.swift
//  city search
//
//  Created by Bence Fulop on 28/10/21.
//

import SwiftUI

struct HorizontalScrollView: View {
    
    let fullContinentName = ["EU": "Europe", "AS": "Asia", "NA": "North America", "AF": "Africa", "AN": "Antarctica", "SA": "South America", "OC": "Oceania"]
    
    let oneRowGrid = [GridItem(.fixed(150))]
    let country: Country
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHGrid(rows: oneRowGrid, spacing: 10) {
                Spacer()
                ScrollCard(cardText: country.capital,
                           iconImage: "building.2",
                           cardColour: .red,
                           textColor: .white)
                ScrollCard(cardText: fullContinentName[country.continent] ?? "Unknown",
                           iconImage: "globe.europe.africa.fill",
                           cardColour: .red,
                           textColor: .white)
                ScrollCard(cardText: country.phoneExtension,
                           iconImage: "phone.fill",
                           cardColour: .red,
                           textColor: .white)
                ScrollCard(cardText: country.currency,
                           iconImage: "banknote",
                           cardColour: .red,
                           textColor: .white)
                Spacer()
            }
        }
    }
}


struct HorizontalScrollView_Previews: PreviewProvider {
    static var previews: some View {
        let country = DataManager().countries.first!
        HorizontalScrollView(country: country)
    }
}
