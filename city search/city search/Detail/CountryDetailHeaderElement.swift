//
//  CountryDetailHeaderElement.swift
//  city search
//
//  Created by Bence Fulop on 28/10/21.
//

import SwiftUI

struct CountryDetailHeaderElement: View {
    var country: Country
    var body: some View {
        VStack {
            Text(country.emoji)
                .font(.custom("system", size: 100))
            Text(country.name)
                .font(.system(size: 40))
                .fontWeight(.bold)
                .lineLimit(3)
                .multilineTextAlignment(.center)
            
            Text(country.native)
                .font(.subheadline)
                .foregroundColor(Color.gray)
                .fontWeight(.light)
                .multilineTextAlignment(.center)
                .padding(.bottom, 50)
        }
    }
}

struct CountryDetailHeaderElement_Previews: PreviewProvider {
    static var previews: some View {
        CountryDetailHeaderElement(country: DataManager().countries.first!)
    }
}
