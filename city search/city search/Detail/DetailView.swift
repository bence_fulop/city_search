//
//  DetailView.swift
//  city search
//
//  Created by Bence Fulop on 25/10/21.
//

import SwiftUI

struct DetailView: View {
    
    let country: Country
    
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [.white.opacity(0.1), .orange.opacity(0.7), .red.opacity(0.8)]), startPoint: .topLeading, endPoint: .bottomTrailing)
                .ignoresSafeArea()
            VStack(spacing: 20) {
                Spacer()
                CountryDetailHeaderElement(country: country)
                Spacer()
                HorizontalScrollView(country: country)
                Spacer()
                OutlineButton(countryName: country.name, buttonText: "See on map")
                Spacer()
            }
        }
    }
}

struct VerticalLabelStyle: LabelStyle {
    func makeBody(configuration: Configuration) -> some View {
        VStack {
            configuration.icon
                .padding(.bottom, 5)
            configuration.title
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(country: DataManager().countries.first!)
    }
}
